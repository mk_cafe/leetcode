/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
    let highestPrice = prices[prices.length - 1];
    let highestProfit = 0;

    // for (let i = prices.length - 1; i >= 0; --i) {
    //     if (prices[i] > highestPrice)
    //         highestPrice = prices[i];
    //     if (highestProfit < highestPrice - prices[i])
    //         highestProfit = highestPrice - prices[i];
    // }

    for (let i = prices.length - 1; i >= 0; --i) {
        highestPrice = Math.max(highestPrice, prices[i]);
        highestProfit = Math.max(highestProfit, highestPrice - prices[i]);
    }

    return highestProfit;
};
