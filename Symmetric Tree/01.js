/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isSymmetric = function(root) {
    const compareSides = (left, right) => {
        if (!left && !right) {
            return true;
        } else if (left && right && left.val === right.val) {
            return compareSides(left.left, right.right) && compareSides(left.right, right.left);
        } else {
            return false;
        }
    }

    return compareSides(root.left, root.right);
};
