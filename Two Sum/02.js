/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    const values = new Map();
    for (const [key, value] of nums.entries()) {
	    const b = target - value;
	    if (values.has(b)) {
            return [key, values.get(b)]
        } else {
            values.set(value, key)
        }
    }
};
