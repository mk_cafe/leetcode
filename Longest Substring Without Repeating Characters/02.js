/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
    let occurences = new Set();
    let l = 0;
    let r = 0;
    let longest = 0;

    for (const ch of s) {
        while (occurences.has(ch)) {
            occurences.delete(s[l]);
            l += 1
        }
        occurences.add(ch);
        longest = Math.max(longest, r - l + 1);
        r++;
    }

    return longest;
};
