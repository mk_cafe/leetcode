/**
 * @param {number[]} nums
 * @return {number[]}
 */
var productExceptSelf = function(nums) {
    let ans = new Array(nums.length).fill(1);
    for(let i = 0, suf = 1, pre = 1, n = nums.length; i < n; i++) {
        ans[i] *= pre;
        pre *= nums[i];
        ans[n-1-i] *= suf;
        suf *= nums[n-1-i];
    }
    return ans;
};
