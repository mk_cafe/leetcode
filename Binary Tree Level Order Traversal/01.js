/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */

/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var levelOrder = function(root) {
     if (!root) {
         return [];
     }

    const queue = [root];

    const r = [];


    while (queue.length) {
        const currentLevel = [];

        for (let size = queue.length; size > 0; --size) {
            const node = queue.shift();

            currentLevel.push(node.val);

            if (node.left) {
                queue.push(node.left);
            }

            if (node.right) {
                queue.push(node.right);
            }
        }

        r.push(currentLevel);
    }

    return r;
};
