/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function(nums, target) {
//     let high = nums.length - 1;
//     let low = 0;

//     while (low <= high) {
//         const mid = Math.floor(low + (high - low)/2);
//         console.log({mid, high, low, target})

//         if (nums[mid] == target) {
//             return mid;
//         }

//         if (nums[mid] < target) {
//             low = mid + 1;
//         } else {
//             high = mid - 1;
//         }
//     }

//     return -1;
    const n = nums.length;
    let lo=0,hi=n-1;
        // find the index of the smallest value using binary search.
        // Loop will terminate since mid < hi, and lo or hi will shrink by at least 1.
        // Proof by contradiction that mid < hi: if mid==hi, then lo==hi and loop would have been terminated.
        while(lo<hi){
            const mid=Math.floor((lo+hi)/2);
            if(nums[mid]>nums[hi]) {
                lo=mid+1
            }
            else {
                 hi=mid;
            }
        }
        // lo==hi is the index of the smallest value and also the number of places rotated.
        const rot=lo;
        lo=0;hi=n-1;
        // The usual binary search and accounting for rotation.
        while(lo<=hi){
            const mid=Math.floor((lo+hi)/2);
            const realmid=(mid+rot)%n;
            if(nums[realmid]==target) {
                return realmid;
            }
            if(nums[realmid]<target)
            {
                lo=mid+1;
            } else {
                hi=mid-1;
            }
        }
        return -1;
};
