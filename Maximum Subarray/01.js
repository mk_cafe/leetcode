/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function(nums) {
    let max_val = nums[0];
    let temp = 0;

        for (const num of nums){
            if (temp < 0) {
                temp = 0
            }
            temp += num;
            max_val = Math.max(max_val, temp);
        }
        return max_val
};