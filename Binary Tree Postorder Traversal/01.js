/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var postorderTraversal = function(root) {
    if (root === null) {
        return [];
    }

    const result = [];

    if (root.left !== null) {
        result.push(...postorderTraversal(root.left))
    }

    if (root.right !== null) {
        result.push(...postorderTraversal(root.right))
    }

    result.push(root.val)

    return result;
};
