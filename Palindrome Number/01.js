/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function(x) {
    let s = x.toString();

    let pal = true;

    for (let i = 0; i < s.length; ++i) {
        if (s[i] !== s[s.length - 1 - i]) {
            pal = false;
            break;
        }
    }

    return pal;

};
