/**
 * Definition for isBadVersion()
 *
 * @param {integer} version number
 * @return {boolean} whether the version is bad
 * isBadVersion = function(version) {
 *     ...
 * };
 */

const binarySearch = (isBadVersion, left, right) => {
    const mid = Math.floor(left + (right - left) / 2);

    if (left > right)
        return -1;

    if (left === right) {
        return mid;
    }

    console.log(isBadVersion(mid), mid);

    if (isBadVersion(mid)) {
        return binarySearch(isBadVersion, left, mid);
    } else {
        return binarySearch(isBadVersion, mid + 1, right);
    }
};

/**
 * @param {function} isBadVersion()
 * @return {function}
 */
var solution = function(isBadVersion) {
    /**
     * @param {integer} n Total versions
     * @return {integer} The first bad version
     */
    return function(n) {
        return binarySearch(isBadVersion, 1, n);
    };
};
