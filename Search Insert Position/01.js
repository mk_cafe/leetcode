/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */

const binarySearch = (arr, x, left, right) => {
    const mid = Math.floor(left + (right - left) / 2) ;

    console.log({mid, left, right, x})

    if (arr[mid] === x) {
        return mid;
    } else
    if (left > right) {
        return left;
    } else
    if (mid < 0) {
        return 0
    } else
    if (mid >= arr.length) {
        return mid;
    } /*else if (arr[mid - 1] < x && arr[mid + 1] > x) {
        return mid;
    }*/

    if (x < arr[mid]) {
        if (x > arr[mid - 1]) return mid;
        return binarySearch(arr, x, left, mid - 1);
    } else {
        if (x < arr[mid + 1]) return mid + 1;
        return binarySearch(arr, x, mid + 1, right);
    }
};

var searchInsert = function(nums, target) {
    return binarySearch(nums, target, 0, nums.length - 1);
};
