/**
 * @param {number[]} nums
 * @return {number}
 */
var findMin = function(nums) {
    const n = nums.length;
    let mini = Infinity;
    let low = 0, high = n-1;
    while(low <= high){
        const mid = Math.floor(low + (high-low)/2);
        if(nums[mid] >= nums[low]){
            mini = Math.min(mini, nums[low]);
            low = mid+1;
        }
        else{
            mini = Math.min(mini, nums[mid]);
            high = mid-1;
        }
    }
    return mini;
};
