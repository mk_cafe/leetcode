/**
 * @param {number[]} nums
 * @return {boolean}
 */
var containsDuplicate = function(nums) {
    const dupl = {};

    for (const num of nums) {
        if (dupl[num]) {
            return true;
        } else {
            dupl[num] = true;
        }
    }

    return false;
};
