/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function(l1, l2) {
    const nodes = new ListNode();

    let carry = 0;

    let current = nodes;



    while (l1 !== null || l2 !== null) {
        // console.log({l1, l2}, typeof l1, typeof l2, l1?.next, l2?.next, typeof l1?.next, typeof l2?.next)
        let sum = (l1?.val || 0) + (l2?.val || 0) + carry;

        console.log({carry, sum, nodes, l1, l2})

        if (sum > 9) {
            carry = 1;
            sum %= 10;
        } else {
            carry = 0;
        }


        current.val = sum;
        current.next = null;

        console.log(l1?.next, l2?.next);
         if (l1?.next !== undefined) {
             l1 = l1?.next
        }
        if (l2?.next !== undefined) {
             l2 = l2?.next
        }
        if (l1 !== null || l2 !== null) {
            current.next = new ListNode();
            current = current.next;
        }
    }

        if (carry) {
            current.next = new ListNode(carry);
        }

    console.log({nodes})

    return nodes;

};
