/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */

const binarySearch = (arr, x, left, right) => {
    const mid = Math.floor(left + (right - left) / 2) ;

    if (arr[mid] === x) {
        return mid;
    }

    if (left > right)
        return -1;

    if (x < arr[mid]) {
        return binarySearch(arr, x, left, mid - 1);
    } else {
        return binarySearch(arr, x, mid + 1, right);
    }
};

var search = function(nums, target) {
    return binarySearch(nums, target, 0, nums.length - 1);
};
